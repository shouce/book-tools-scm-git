# 基本操作

```
$ git help
用法：git [--version] [--help] [-C <path>] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

最常用的 git 命令有：
   add        添加文件内容至索引
   bisect     通过二分查找定位引入 bug 的变更
   branch     列出、创建或删除分支
   checkout   检出一个分支或路径到工作区
   clone      克隆一个版本库到一个新目录
   commit     记录变更到版本库
   diff       显示提交之间、提交和工作区之间等的差异
   fetch      从另外一个版本库下载对象和引用
   grep       输出和模式匹配的行
   init       创建一个空的 Git 版本库或重新初始化一个已存在的版本库
   log        显示提交日志
   merge      合并两个或更多开发历史
   mv         移动或重命名一个文件、目录或符号链接
   pull       获取并整合另外的版本库或一个本地分支
   push       更新远程引用和相关的对象
   rebase     本地提交转移至更新后的上游分支中
   reset      重置当前HEAD到指定状态
   rm         从工作区和索引中删除文件
   show       显示各种类型的对象
   status     显示工作区状态
   tag        创建、列出、删除或校验一个GPG签名的 tag 对象

命令 'git help -a' 和 'git help -g' 显示可用的子命令和一些指南。参见
'git help <命令>' 或 'git help <指南>' 来查看给定的子命令帮助或指南。
```

### 缓存区

- add 将文件或目录添加到缓存区  
   `git add .` 添加所有文件  
    git 不受理空目录哦～，按照惯例，通常都是先在空目录中添加 .gitkeep 文件来让空目录不空的，呵呵。  
    提示：`find . -type d -empty -not -path '*/\.*' -exec touch {}/.gitkeep \;` 在所有的空目录中添加 .gitkeep 文件。
- rm 删除文件
   `git rm -r *` 递归删除
- mv 移动文件（重命名）
- stash 暂存

   ```
   git stash                    # 暂存当前修改，将所有至为HEAD状态
   git stash list               # 查看所有暂存
   git stash show -p stash@{0}  # 参考第一次暂存
   git stash apply stash@{0}    # 应用第一次暂存
   ```

### 本地仓库

- `init` 初始化本地仓库/建立版本数据库
- `commit` 将缓存区文件提交到仓库  
  `git commit -am "..."` 合并了 add 操作的提交  
  `git commit -amend -m "..."` 合并上一次提交（用于反复修改）
- `reset` 将 HEAD 移动到指定状态
- `revert` 在HEAD的基础上恢复指定提交（即去掉此次提交产生的影响），然后建立一个新的提交

### 远程仓库

- clone 克隆远程仓库
  - `git clone git+ssh://git@192.168.53.168/VT.git`
- remote 远程仓库管理
  - `git remote -v`
  - `git remote add  远程库名称  地址`
  - `git remote rm 远程库名称`
  - `git remote set-url  远程库名称 地址` 设置远程地址
  - 获取远程库路径：`git config --get remote.origin.url`
- pull 从远程仓库或者本地分支拉取代码
  - `git pull origin master` 获取（fetch）远程分支 origin/master 并合并（merge）到当前分支
- push 推送
  - `git push origin <local-branch>:<remote-branch>` 推送到远程分支
  - `git push origin :<remote-branch>` 删除远程分支
- fetch Download objects and refs from another repository
  - `git fetch` 获取所有远程分支（不更新本地分支，另需 merge）
  - `git fetch --prune` 获取所有原创分支并清除服务器上已删掉的分支  

  网上看到有些博客中说通过 `git clone --bare` 命令克隆的裸仓库没有远程仓库，不过经过验证是有的（不排除版本问题）。  
  通过 `git config --list` 可以查看到：  
  `remote.origin.url=<remote_url>`  
  `remote.origin.fetch=+refs/heads/*:refs/remotes/origin/*`  
  可以手动配置 `git config remote.origin.fetch=+refs/*:refs/*` 来匹配所有改动  
  `remote.origin.mirror=true` 作用不明  

### 分支和标签

- branch 分支
  - `git branch`  显示本地分支，参数：a/r/l/v/merged/no-merged...
  - `git branch --contains <commit_id>/<branch_name>` 包含某次提交，也可以用于搜索分支
  - `git branch add dev`
  - `git branch delete dev`
  - `git branch -m master master_copy` 本地分支改名
  - `git branch --track experimental origin/experimental`    跟踪分支，例如克隆的时候会自动创建一个和远程版本库当前分支同名的跟踪分支
  - `git branch --set-upstream-to=origin/master master` 自定义跟踪分支
- tag 标签
  - `git tag` 显示已存在的tag
  - `git tag -a v2.0 -m 'xxx'` 增加v2.0的tag
  ```
  git tag [-a | -s | -u <key-id>] [-f] [-m <msg> | -F <file>]
          <tagname> [<commit> | <object>]
  git tag -d <tagname>…
  git tag [-n[<num>]] -l [--contains <commit>] [--points-at <object>]
          [--column[=<options>] | --no-column] [<pattern>…]
          [<pattern>…]
  git tag -v <tagname>…
  ```
- checkout 切换分支（其实本质是修改工作目录，将版本库中的指定文件或目录覆盖到工作目录）
  `git checkout features/performance` 检出已存在的features/performance分支
  `git checkout --track origin/hotfixes` 检出远程分支并创建本地跟踪分支
  `git checkout -- deleted_file` 恢复删除文件，常用于修改错误回退
  `git checkout dev new_file` 从其他分支复制一个文件过来
  `git checkout -b master new_branch` 从指定分支（master）创建新分支（new_branch）并检出
  `git checkout -b new_branch` 如果不指定检出分支，则默认从当前分支检出
- merge
- rebase

### 其他

- reflog 引用移动的日志，每一次提交、reset、revert、checkout、merge 等都产生一次引用移动
  也就是说所有孤立的提交节点都会显示出来。
  `HEAD@{1}` = `ORIG_HEAD`
- log 查看日志，参数：p/m/stat/graph...
  搜索，参见：git-log.html#_commit_limiting
  `git log --oneline -10`
  `git log --oneline -n 10`
  `git log --oneline --max-count=10`
  `git log --author=catroll` 根据用户名查找提交
  `git log --pretty=format:'%h %s' --graph` 图示提交日志
- show 查看详情
  `git show dfb02/HEAD/HEAD^2/HEAD~3/HEAD@{5}` 显示某个提交的详细内容
  `git show master@{yesterday}` 显示 master 分支昨天的状态
  `git show -s --pretty=raw v2.0` 显示 v2.0 的日志及详细内容
- `show-branch --all` 显示分支历史
- ls-files 列出暂存区文件
- status Show the working tree status
- diff 比对不同提交之间的差异或者工作区和提交之间的差异等
  ```
  git diff                               # 显示所有未添加至index的变更
  git diff --cached                      # 显示所有已添加index但还未commit的变更
  git diff HEAD^                         # 比较与上一个版本的差异
  git diff HEAD -- ./lib                 # 比较与HEAD版本lib目录的差异
  git diff origin/master..master         # 比较远程分支master上有本地分支master上没有的
  git diff origin/master..master --stat  # 只显示差异的文件，不显示具体内容
  git diff --cached main.py
  git diff --staged main.py
  git diff 6163da3 main.py
  ```
- grep 所有文件中搜索指定字符串（支持正则）

## 四、进阶

```
# git help -a
available git commands in 'C:\msysgit/libexec/git-core'

  add                  gui                  receive-pack
  add--interactive     gui--askpass         reflog
  am                   gui--askyesno        relink
  annotate             gui.tcl              remote
  apply                hash-object          remote-ext
  archimport           help                 remote-fd
  archive              http-backend         remote-ftp
  bisect               http-fetch           remote-ftps
  bisect--helper       imap-send            remote-http
  blame                index-pack           remote-https
  branch               init                 remote-testsvn
  bundle               init-db              repack
  cat-file             instaweb             replace
  check-attr           log                  repo-config
  check-ignore         lost-found           request-pull
  check-mailmap        ls-files             rerere
  check-ref-format     ls-remote            reset
  checkout             ls-tree              rev-list
  checkout-index       mailinfo             rev-parse
  cherry               mailsplit            revert
  cherry-pick          merge                rm
  citool               merge-base           send-email
  clean                merge-file           send-pack
  clone                merge-index          sh-i18n
  column               merge-octopus        sh-i18n--envsubst
  commit               merge-one-file       sh-setup
  commit-tree          merge-ours           shell
  config               merge-recursive      shortlog
  count-objects        merge-resolve        show
  credential           merge-subtree        show-branch
  credential-store     merge-tree           show-index
  cvsexportcommit      mergetool            show-ref
  cvsimport            mergetool--lib       stage
  cvsserver            mktag                stash
  daemon               mktree               status
  describe             mv                   stripspace
  diff                 name-rev             submodule
  diff-files           notes                svn
  diff-index           p4                   symbolic-ref
  diff-tree            pack-objects         tag
  difftool             pack-redundant       tar-tree
  difftool--helper     pack-refs            unpack-file
  fast-export          parse-remote         unpack-objects
  fast-import          patch-id             update-index
  fetch                peek-remote          update-ref
  fetch-pack           prune                update-server-info
  filter-branch        prune-packed         upload-archive
  fmt-merge-msg        pull                 upload-pack
  for-each-ref         push                 var
  format-patch         quiltimport          verify-pack
  fsck                 read-tree            verify-tag
  fsck-objects         rebase               web--browse
  gc                   rebase--am           whatchanged
  get-tar-commit-id    rebase--interactive  write-tree
  grep                 rebase--merge
```

- `git gc`    Cleanup unnecessary files and optimize the local repository
- `git fsck`  Verifies the connectivity and validity of the objects in the database
- `bisect`    Find by binary search the change that introduced a bug
- `rebase`    Forward-port local commits to the updated upstream head

## 五、更多

```
# git help -a
...
# git help -g
The common Git guides are:

   attributes   Defining attributes per path
   glossary     A Git glossary
   ignore       Specifies intentionally untracked files to ignore
   modules      Defining submodule properties
   revisions    Specifying revisions and ranges for Git
   tutorial     A tutorial introduction to Git (for version 1.5.1 or newer)
   workflows    An overview of recommended workflows with Git
```

问题：push 时遇到 `error: src refspec XXX matches more than one`

因为 tag 和 branch 同名，比如都是叫做 v6.4
这时 tag 和 branch 应该使用完整的名称，以便区分：refs/tags/v6.4、refs/heads/v6.4
比如：

```sh
# 推送到远程 v6.4 分支
git push github refs/heads/v6.4

# 删除远程 v6.4 标签
git push github :refs/tags/v6.4
```

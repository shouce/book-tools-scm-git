```
add                 checkout            config              gc                  mergetool           remote              st
am                  cherry              credential          get-tar-commit-id   mv                  repack              stage
annotate            cherry-pick         describe            grep                name-rev            replace             stash
apply               ci                  df                  gui                 notes               request-pull        status
archive             citool              diff                help                oneline             reset               submodule
bisect              clean               difftool            imap-send           pull                revert              subtree
blame               clone               fetch               init                push                rm                  svn
br                  co                  filter-branch       instaweb            rebase              shortlog            tag
branch              column              format-patch        log                 reflog              show                verify-commit
bundle              commit              fsck                merge               relink              show-branch         whatchanged
```

1. am
2. annotate
3. apply
4. archive
5. bisect
6. blame
7. bundle
9. cherry Find commits yet to be applied to upstream
10. cherry-pick Apply the changes introduced by some existing commits
11. column
12. credential
13. filter-branch Rewrite branches
14. format-patch
15. fsck Verifies the connectivity and validity of the objects in the database
16. gc 优化本地仓库
17. get-tar-commit-id
18. imap-send
19. name-rev
20. relink
21. repack
22. request-pull
23. show-branch
24. submodule
25. subtree
26. verify-commit


- <http://blog.dayanjia.com/2014/03/git-101/>
- <http://ixirong.com/2014/11/19/the-way-to-learn-git/>
- <http://liujin.me/blog/2015/05/25/Git-常用命令/>


# sync

用于同步子模块的远程 URL。

这对于修改 `.gitmodules` 文件之后的子模块操作很有必要。
`git` 需要使用 `submodule sync` 命令将 `.gitmodules` 中的子模块地址同步到本地仓库（`.git`）信息中去。

比如这份 git 笔记所在仓库包含于[另一个仓库](`https://gitee.com/shouce/shouce.git`)。
假设 submodules 中记录的 URL 是 `https://gitee.com/shouce/book-tools-scm-git.git`，而实际仓库远程 URL 是 `http://gitee.com/shouce/book-tools-scm-git`，那么：

在父仓库中执行 `git submodule sync` 之后，再看，本地仓库远程 URL 就更新为 submodules 中的配置了。

可以使用以下脚本检查所有仓库的 origin URL：

```sh
for i in `find ~/Documents/Shouce -name ".git" -not \( -path "*/_book/*" -prune \)`;
do
    cd `dirname $i`;
    git remote get-url --push origin;
    cd ~/Documents/Shouce/;
done;
```

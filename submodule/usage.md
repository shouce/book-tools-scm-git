# 基本操作

## 克隆

参考：https://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules


```
git clone --recurse-submodules -j8 github.com:shouce/shouce.git # 2.13+
git clone --recursive -j8 github.com:shouce/shouce.git # 1.6.5+
```

`-j` 表示子模块并发操作，每次 n 个子模块。

针对更老的版本或者以存在的库：

```sh
git clone github.com:shouce/shouce.git
cd shouce
git submodule update --init --recursive
```

发现一个 clone 参数 `--[no-]shallow-submodules`，可以使每个子模块仓库的克隆 deepth 为 1，应该是用得上的。

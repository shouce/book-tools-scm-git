# Bugs

在 Git 1.8.5 上面如果 clone 自 `https://xxx/xxx.git` 则在命令行下进行远程操作（如 pull、push）会出现如下错误信息：`fatal: could not read Username for 'https://git.oschina.net': No such file or directory`。
经测试 SSH 连接（`ssh -T git.oschina.net`）是通的。网上搜索到的所有帖子都是认为这是 Git 1.8.5 的漏洞，在 Git 1.8.4 下没有这个问题，要么修改成 SSH 方式的远程地址，要么使用新版本。


## git gui 拼写检查报错

安装了git-gui，打开以后出现以下提示：

```
Spell checking is unavable:

error：No word lists can be found for the language "zh_CN"
```

原因：打开的时候会进行拼写检查。

解决方法:

`gedit  ~/.gitconfig`，然后在文件末尾追加以下内容：

```
[gui]
      spellingdictionary = none
```

## 出现两个 origin

```
catroll@CatRoll MINGW64 /d/Projects/scripts (master)
$ git br -vva
* master                82ad20f [remotes/origin/master] xxxxxx
  origin/master         eb2b7da yyyyyy
  remotes/origin/master 82ad20f xxxxxx
```

不知道为什么出现这种情况。

查看 `.git/refs` 发现多出来个 `.git/refs/heads/origin/master`。

`.git/refs/heads/` 下面应该是本地引用的，远端应用应该都在 `.git/refs/remotes/` 下才对。

直接 `rm -rf .git/refs/heads/origin` 就好了。

